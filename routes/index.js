var express = require('express');
var router = express.Router();
var fs = require("fs")

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/upload', function(req, res, next) {

  if(req.files){

    if(req.files.attachment){

      let uploadedFile = req.files.attachment

      let extension = uploadedFile.name.split('.').pop()

      let cleanFileName = uploadedFile.name.replace(/[^a-zA-Z0-9]/g, "")

      let fileName = `user_content/${(new Date()).getTime()}-${cleanFileName}.${extension}`

      uploadedFile.mv(fileName, (err)=>{
        if(err){
          return res.status(500).send({
            success : false,
            message : err
          })
        }else{

          //TODO : Menambahkan logic utk store string ini (filename) ke database

          return res.send({location : fileName})
        }
      })

    }else{
      return res.status(400).send({
        success : false,
        message : "Please upload an attachment"
      })
    }

    // res.status(200).send({
    //   success:true
    // })

  }else{
    res.status(400).send({
      success : false,
      message : "Please upload an attachment"
    })
  }

});

router.get('/getFile', function(req, res, next) {

  let filePath = String(req.query.path)

  if(filePath){

    fs.access(filePath, function (err) {

      if(err){
        return res.status(500).send({
          success : false,
          message : err
        })
      }else{
        fs.createReadStream(filePath).pipe(res)
      }

    })

  }else{
    return res.status(400).send({
      success : false,
      message : "Please enter a path"
    })
  }

});

module.exports = router;
